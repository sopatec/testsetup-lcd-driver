#/bin/bash

SCRIPTPATH="$(dirname $(readlink -f "$0"))"
cd "$SCRIPTPATH"

if [ -z "$PORT" ]; then
    PORT="/dev/ttyUSB0"
fi

function compile() {
    arduino-cli compile --fqbn arduino:avr:pro
}

function upload() {
    arduino-cli upload --fqbn arduino:avr:pro --port $PORT
}

function watch() {
    stty -F $PORT raw 57600
    cat $PORT
}

if [ $# -ge 1 ]; then
    $1
else
    set -e
    compile
    upload
    [ $? -ne 0 ] && exit 1
    sleep 1
    watch
fi
