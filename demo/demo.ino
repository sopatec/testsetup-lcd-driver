/* Sample setup for an arduino pro mini running at 3.3V and 8 MHz (schematic in /docs) */

#include "Arduino.h"
#include "uc1698u.h"
#include <avr/pgmspace.h>

#include "img.h"

struct uc1698u_config config = {
	.pin = {
		.CS = 13,
		.CD = 10,
		.WR0 = 11,
		.WR1 = 12,
		.DX = {9, A1, 8, A0, 7, 4, 6, 5 } /* not using pins 2, 3 because of interrupts */
	},
	.state = uc1698u_default_state
};

void
setup()
{
	Serial.begin(115200);
	while (!Serial) {}

	uc1698u_init_pins(&config);
	Serial.write("Init pins done!\n\r");

	uc1698u_init_erc160160(&config);
	Serial.write("Init lcd done!\n\r");

	uc1698u_set_lcd_bias_ratio(config, UC1698U_LCD_BIAS_RATIO_11)
	// ^^^ Changes lcd bias ratio.. possible ratios:
	// - UC1698U_LCD_BIAS_RATIO_5
	// - UC1698U_LCD_BIAS_RATIO_10
	// - UC1698U_LCD_BIAS_RATIO_11
	// - UC1698U_LCD_BIAS_RATIO_12

	uc1698u_write_image_64K(&config, img, 0, 0, 160, 160);
	Serial.write("Writing image done!\n\r");

	uc1698u_wake_display(&config);
	Serial.write("Display wake done!\n\r");
}

void
loop()
{
	static int potval, slept = 0, scroll = 0;
	static const int sleep = 40, updatedelay = 500;

	scroll = (scroll + 20 + rand() % 120) % 160;
	uc1698u_set_scroll_line(&config, scroll);
	uc1698u_set_lcd_mapping_control(&config, UC1698U_PARTIAL_DISPLAY_CONTROL_DISABLE
	//	| UC1698U_LCD_MIRROR_X_ENABLE * (rand() % 2) // only changes write order! :(
		| UC1698U_LCD_MIRROR_Y_ENABLE * (rand() % 2));
	// ram_address_control only changes write order!
	// uc1698u_set_ram_address_control(&config, UC1698U_AUTO_COL_ROW_WRAPAROUND_ENABLE
	// 	| UC1698U_AUTO_INCREMENT_ROW_FIRST * (rand() % 2)
	// 	| UC1698U_ROW_ADDRESS_AUTO_INCREMENT_NEG * (rand() % 2));

	delay(sleep); /* bit faster than 20fps */

	if ((slept += sleep) > updatedelay) {
		slept %= updatedelay;
		potval = analogRead(A2);
		uc1698u_set_vbias_pot(&config, potval);
		Serial.println(potval);
	}
}
