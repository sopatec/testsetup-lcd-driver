#!/bin/bash

REPOROOT=$(git rev-parse --show-toplevel)

docker build -t test-setup-upload-env -f "$REPOROOT/extra/env/Dockerfile" $REPOROOT
