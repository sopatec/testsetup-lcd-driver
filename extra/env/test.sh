#!/bin/bash

REPOROOT=$(git rev-parse --show-toplevel)

if [ $# -lt 1 ]; then
    echo "Usage: test.sh <device-path>"
    exit 1
fi

docker run -it -v $REPOROOT:/lcd-driver -e PORT=$1 --device=$1 test-setup-upload-env
